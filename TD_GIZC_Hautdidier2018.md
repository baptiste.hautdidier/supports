![](Figures/Header2024.png)

__Occupations du sol et multi-usage de l’espace autour du Bassin d’Arcachon__  
Baptiste Hautdidier  
Support TD 'GIZC', 2024-2025  
29/11/2024

[[_TOC_]]

## Pour cette séance
Fichier `Données.zip` à télécharger [via le service Renater](https://filesender.renater.fr/?s=download&token=b9cea1fe-ea58-419e-be77-d19e0f4437c2).


***

## 🚧 Exercice 1 - Construire l'emprise d'une zone d'étude en partant de découpages administratifs

### 🗃 Données nécessaires

* _Shapes_ : `Communes33_40.shp`

Nous allons nous intéresser au [pays](https://anpp.fr/payspetr/definition-pays-petr/) 'Bassin d'Arcachon Val de l'Eyre'(BARVAL), un territoire administratif intercommunal dont la maille est le support d'actions publiques tournées vers la planification et l'urbanisme : le périmètre du pays BARVAL coincide notamment avec un *Schéma de Cohérence Territoriale* (SCOT) du même nom, dont l'influence a pu être forte sur certaines problématiques de conservation (logique Trame Verte et Bleue...). Le territoire bénéficie entre 2023 et 2025 d'un [contrat de développement et de transitions](https://territoires.nouvelle-aquitaine.fr/documents-strategiques/la-strategie-de-developpement-du-bassin-darcachon-val-de-leyre). La liste des communes membres du pays BARVAL peut être construite par l'union des structures intercommunales [EPCI](http://www.insee.fr/fr/methodes/default.asp?page=definitions/etab-pub-cooper-intercom.htm) suivantes :  

EPCI                                                                     | N° SIREN
-------------------------------------------------------------------------|----------
Communauté d'Agglomération Bassin d'Arcachon Sud-Pôle Atlantique (COBAS) | 243300563
Communauté de communes du Val de l'Eyre                                  | 243301405
Communauté de communes du Bassin d'Arcachon Nord Atlantique (COBAN)      | 243301504

Le code SIREN est un identifiant unique de ces structures.

### 🧭 Consignes

1. Charger dans ArcGIS la couche décrivant les géométries communales des départements des Landes et de la Gironde (fichier `Communes33_40.shp`, source [BD Carto](http://professionnels.ign.fr/bdcarto)). 
2. Construire une requête sur le champ `SIREN_EPCI`, qui selectionne les communes du seul pays 'Bassin d'Arcachon Val de l'Eyre' en se basant sur les codes SIREN comme attributs. 
(`Carte > Sélection > Sélectionner selon les attributs`. La requête SQL est de la forme: `SELECT * FROM Communes33_40 WHERE "Champ"='valeur1' OR "Champ"='valeur2'` ...)
3. Fusionner les entités sélectionnées, de manière à obtenir une nouvelle couche avec une seule entité, et enregistrer cette couche. Exporter préalablement la sélection. Puis utiliser `Fusionner` (ou _dissolve_ en VO) (dans la boîte à outils ou _toolbox_ : `Outils de gestion des données > Généralisation > Fusionner`), en se basant sur un champ dont les valeurs sont connues comme étant identiques sur la zone.


<details>

<summary>

__⚠️ Les outils de géotraitement dans ArcGIS Pro__

</summary>

... sont accessibles de plusieurs manières :

* soit partiellement par les menus : `Analyse > Outils`
* soit par le volet de la boite à outils : `géotraitement > boite à Outils`


![](Figures/geotraitements.png)<!-- .element height="20%" width="20%" -->

</details>


***
    
## 🚧 Exercice 2 - Analyser la construction des emprises des parcs naturels de la zone

### 🗃 Données nécessaires

* _Shapes_ : `Communes33_40.shp`, `pnm2014_07.shp`, `pnr2008.shp`,`Contour_PnrLG_2014_2026.shp`
* Tables : `CommunesPNR.xls`

Le bassin d'Arcachon est concerné par deux territoires de conservation construits par contractualisation avec des collectivités territoriales : 
un parc naturel marin (PNM) et un parc naturel régional (PNR). La délimitation de leurs emprises est l'aboutissement de longues négociations avec les communes et structures intercommunales concernées. Le PNR a fait l'objet d'une révision de sa charte en 2013. Le dispositif PNM a lui été validé en juin 2014. 

### 🧭 Consignes

1. Reprendre les géométries communales (`Communes33_40.shp`, source BD Carto).
2. Ouvrir `CommunesPNR.xls` dans un tableur : que semble décrire le fichier ?   
3. Joindre cette table à la couche décrivant les géométries communales. Attention aux formats des champs utilisés pour la jointure. 
4. Fusionner en se basant sur les nouvelles informations tabulaires récupérées grace à la jointure, de manière à construire une carte avec une symbologie montrant l'évolution du zonage du PNR (le plus simple est d'utiliser deux champs pour la fusion). Charger `pnr2008.shp` (un export de l'[INPN](https://inpn.mnhn.fr/accueil/index) montrant l'emprise officielle du parc en 2008) et inspecter visuellement les différences. 
5. Charger la couche `pnm2014_07`. Quelles formes de recouvrements entre `pnr2008.shp` et `pnm2014_07` observe-t-on au fond du bassin d'Arcachon ? Charger `Contour_PnrLG_2014_2026.shp`, qui comme son nom l'indique est le contour du parc pour la période de la charte en vigueur, 2014-2026 (Voir l'article 7 dans [LégiFrance](http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025199018&dateTexte=&categorieLien=id)). Inspecter également la nature du recouvrement entre les dispositif PNM et PNR (2008 et actuel).
6. Se connecter en flux WMS à l'infrastructure de données spatiales régionale PIGMA (`Insérer > Connexions > Serveur > nouveau serveur WMS`). L'URL à copier est : https://www.pigma.org/geoserver/wms. Ajouter la couche décrivant le fuseau du projet de ligne à grande vitesse Bordeaux-Dax-Toulouse (choisir la couche `RFF - Projet LGV GPSO 1000m` en naviguant dans l'arborescence du Catalogue). En complément, consulter le tracé définitif des LGV Bordeaux–Toulouse et Bordeaux– Espagne sur le [site du Grand Projet Ferroviaire du Sud-Ouest](https://experience.arcgis.com/experience/41fb2de691c34112ae63f823255ac82d/). Zoomer sur l'intersection du fuseau avec le secteur Nord-Est du PNR. Quelles hypothèses peut-on faire sur les raisons probables des entrées de communes dans le PNR des Landes de Gascogne ?
7. Initialement prévue pour 2014-2026, l'application de la charte a été étendue jusqu'à 2029. le travail de révision de la charte a commencé fin 2024 : consulter le [feuillet n° 1 de la révision de la charte](https://www.parc-landes-de-gascogne.fr/Parc-Naturel-Regional-de-Gascogne/VIVRE/Vous-habitez-le-Parc/Parlons-territoire-participez-a-la-revision-de-la-Charte/Les-feuillets-de-la-revision). Que peut-on dire de l'évolution du périmètre d'étude ?  


<details>

<summary>

__⚠️ Jointures sur codes INSEE__

</summary>

Ces codes (~numériques à 5 chiffres) des communes ont l'avantage d'être des identifiants uniques. Un logiciel comme Excel peut avoir tendance à les interpréter ces codes comme des nombres entiers. Ils doivent pourtant être stockés et traités sous forme de chaines de caractères ! (Penser aux premiers départements -Ain à Ariège- et au cas de la Corse -2A/2B). Une manipulation sur le fichier Excel permet d'aider ArcGIS à traiter cette colonne au format `string`, il suffit d'insérer un ' devant le premier enregistrement. Alternativement, la jointure pourrait fonctionner sur les noms des communes : c'est toutefois à éviter (trop de sensibilité aux erreurs d'accent et d'encodage).

</details>

<details>

<summary>

__⚠️ Ajouter un serveur WMS__

</summary>

![](Figures/WMS.png)<!-- .element height="20%" width="20%" -->

</details>



***
## 🚧 Exercice 3 - Confronter les zonages politico-administratifs avec la logique 'bassin versant'

### 🗃 Données nécessaires

* _Shapes_: `Communes33_40.shp`, `Secteurs33_40.shp`

### 🧭 Consignes

1. Charger les couches des _secteurs hydrographiques_ issues de la BD Carthage (Voir sur [EauFrance](http://www.glossaire.eaufrance.fr/concept/secteur-hydrographique) pour une définition). Les secteurs hydrographiques correspondent au regroupement des _bassins versants topographiques_ qui ont été définis dans le nouveau référentiel hydrographique français, la [BD TOPAGE](https://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/7fa4c224-fe38-4e2c-846d-dcc2fa7ef73e).
2. Faire une sélection spatiale des secteurs hydrographiques intersectés par l'emprise BARVAL créée pour l'exercice 1 (`Carte > Sélection > Sélectionner selon l'emplacement...`).
3. Exporter cette sélection sous forme d'une nouvelle couche.
4. Mettre en relation ces géométries des secteurs hydrographiques avec celle de la zone d'étude par un géotraitement : créer une nouvelle couche dédiée qui par agrégation (ou _union_ en VO) hérite des géométries et attributs des deux couches (`Boîtes à outils > Outils d'analyse > Superposition > Agréger`).
5. Construire un tableau récapitulant pour les différents secteurs hydrographiques les surfaces (en ha) situées dans _et_ hors la zone d'étude : vérifier que les surfaces sont bien calculées dans un champ dédié, exporter la table attributaire (par exemple au format texte CSV, en donnant un suffixe `*.csv` à votre fichier lors de l'export) et faire le traitement dans un tableur par un tableau croisé dynamique. Dans quelle mesure la logique des découpages administratifs est-elle en décalage avec le biophysique ? Est-ce un problème ?  

</details>

<details>

<summary>

__⚠️ Le calcul des superficies__

</summary>

Il est automatique lorsqu'on travaille en format _geodatabase_ `*.gdb` : c'est un des intérêts d'utiliser ce standard par défaut en travaillant dans un environnement ESRI.
Mais cela requiert une manipulation manuelle quand on part d'un _shapefile_ (créer un champ et appliquer l'outil `Calculer la géométrie`).

</details>

***

## 🚧 Exercice 4 - Caractériser l'occupation du sol d'un bassin versant

### 🗃 Données nécessaires

* _Shapes_: `CLC18_Barval_RGF.shp`
* Symbologie: `CORINE_Land_Cover_06_Vecteur.lyr`

_CORINE Land Cover_ est la base de données pan-Européenne décrivant les changements d'occupation des sols entre les millésimes 1990, 200, 2006, 2012 et 2018. 
Voir [ici](http://www.statistiques.developpement-durable.gouv.fr/donnees-ligne/li/1825.html) pour une description du produit et [là](http://www.statistiques.developpement-durable.gouv.fr/donnees-ligne/t/nomenclature.html) pour le détail des postes de la nomenclature.

![](Figures/CLC.png)<!-- .element height="20%" width="20%" -->
> Les 44 postes de la nomenclature CORINE Land Cover*

### 🧭 Consignes

1. Découper le _shapefile_ _CORINE Land Cover_ par l'emprise constituée par les 4 secteurs hydrographiques (couche sauvegardée au 3.3). Réaliser le découpage (ou _clip_ en VO) en _un seul_ traitement (`Boîtes à outils > Outils d'analyse > Extraire > Découper`). 
2. Appliquer une symbologie grace au fichier `*.lyr` présent dans le dossier. En haut à droite de la fenêtre de l'onglet `symbologie`, utiliser le bouton `importer...` (le champ`CODE_06` du `*.lyr` peut être apparié au `CODE_18` du `*.shp`, les nomenclatures n'ont pas changé entre les différents millésimes).
3. Sélectionner le secteur hydrologique de la Leyre (S2) et l'utiliser pour faire une nouvelle découpe dans la couche _CORINE Land Cover_.
4. Y calculer les surfaces en hectares de chaque type d'occupation du sol (Cf. exercice 3.5) - mais cette fois-ci en réalisant une fusion (_dissolve_) préalable sur le champ de la nomenclature CLC (`Boîtes à outils > Outils de gestion des données > Généralisation > Fusionner`) et en inspectant la table attributaire.



<details>

<summary>

__⚠️ Le traitement par lot__

</summary>

![](Figures/Decouper.png)
Bien utile... Il est accessible par un clic-droit sur l'outil _ArcToolbox_.*

</details>



***

## 🚧 Exercice 5 - Qualifier les transitions identifiées avec la base _CORINE Land Cover_

### 🗃 Données nécessaires 

* _Shapes_: `CHA9000_Barval_RGF.shp, CHA0006_Barval_RGF.shp, CHA0612_Barval_RGF.shp, CHA1218_Barval_RGF.shp`
* Tables: `LCF lookup.xls`

Introduite dans une [étude de l'Agence Européenne pour l'Environnement](http://www.eea.europa.eu/data-and-maps/data/land-cover-accounts-leac-based-on-corine-land-cover-changes-database-1990-2000), 
cette nomenclature propose une simplification de l'ensemble des transitions CLC en neuf grands types, que leurs promoteurs ont appelé des _Land Cover Flows_ (ou LCF).

LCF | Description
----|------------------------------------------------------
1   | _Urban land management_
2   | _Urban residential sprawl_ (étalement urbain résidentiel)
3   | _Sprawl of economic sites and infrastructures_
4   | _Agriculture internal conversions_
5   | _Conversion from forested & natural land to agriculture_
6   | _Withdrawal of farming_
7   | _Forests creation and management_
8   | _Water bodies creation and management_
9   | _Changes of Land Cover due to natural and multiple causes_
> Les 9 _Land Cover Flows_ de l'[agence européenne pour l'environnement](https://www.eea.europa.eu/fr) (AEE)

### 🧭 Consignes
1. Charger et découper avec l'emprise du pays BARVAL les fichiers _shapes_ (`CHAXXX...`) correspondant aux changements d'occupation du sol sur les périodes 1990-2000, 2000-2006, 2006-2012 et 2012-2018. (traitement par lot possible)
2. Inspecter leurs tables attributaires. Sachant que la nomenclature CORINE a 44 postes, quel est le nombre total de types de transitions possibles entre deux millésimes ? 
3. Joindre aux quatre couches `CHA...` la table `LCF lookup.xls` après l'avoir inspectée sous un tableur. 
4. Récapituler les surfaces des différents LCFs sur la zone d'étude pour les différentes périodes. Qu'en conclure, au vu des flux les plus dominants? Quelle est la part probable des changements d'usage associés aux changements d'occupation du sol mesurés via _CORINE Land Cover_ ?  


***

## 🚧 Exercice 6 - Construire des taches urbaines à partir de différentes bases 'OccSol'

### 🗃 Données nécessaires 

* _Shapes_: `CLC18_Barval_RGF.shp`, `IPLI_1977.shp`, `LittoMOS_2006.shp`

Sur le littoral français métropolitain, deux efforts historiques de cartographie exhaustive des occupations du sol sont à noter. Ils sont tous les deux téléchargeables en accès libre :
* l'_Inventaire permanent du LIttoral_ réalisé en 1977 (ou IPLI77, à consulter [sur le géocatalogue](https://www.geocatalogue.fr/geonetwork/srv/fre/catalog.search#/metadata/33bcaa2f-f2f0-4586-8f8e-9b30691ae0eb) ou télécharger [ici](https://geolittoral.din.developpement-durable.gouv.fr/telechargement/couches_sig/N_ipli_77_S_092014_shape.zip))
* le _Mode d'occupation du sol sur le littoral_, basé sur des données de 2000 et 2006 (ou LittoMOS, à consulter [sur le géocatalogue](https://www.geocatalogue.fr/geonetwork/srv/fre/catalog.search#/metadata/f63f945a-8295-40b0-90e3-83c1cff208d4), [sur arcgis.com](https://arcg.is/1buH0q), ou télécharger [ici](https://geolittoral.din.developpement-durable.gouv.fr/telechargement/couches_sig/N_littomos_S_092014_shape.zip)).

### 🧭 Consignes

1. Reprendre le _shapefile_ de _CORINE Land Cover_ 2018 découpé sur l'emprise du pays BARVAL.
2. La nomenclature CORINE est à trois niveaux hiérarchiques (Par ex.: `111 Tissu urbain continu, 11 Tissu urbain, 1 Territoires artificialisés...`). Créer un nouveau champ, dont les valeurs reprennent uniquement le 1<sup>er</sup> niveau. Utiliser par exemple une expression de type `MyStr = Left($feature.Monchamp, nb_de_charactères_à_garder)` dans l'outil `Calculer un champ`.
3. Sélectionner les 'territoires artificialisés', exporter sous la forme d'un nouveau _shape_.
4. Charger les couches `IPLI_1977.shp` et `LittoMOS_2006.shp`. En vérifier rapidement les nomenclatures : elles sont compatibles avec CORINE mais avec un niveau hiérarchique supplémentaire. (Noter que certaines entrées de la nomenclature sont moins développées que dans _CORINE Land cover_). 
5. Construire des couches 'territoires artificialisés' sur le même principe. Superposer les trois produits, les inspecter visuellement. Qu'observe-t-on?




<details>

<summary>

__⚠️ Et sur le littoral néo-Aquitain...__

</summary>

Il est à noter que des produits 'Occupation du sol' plus récents ont été développés en Nouvelle Aquitaine (sur une initiative du GIP littoral reprise par PIGMA et la Région). Ils se distinguent par des nomenclatures spécifiques mais partiellement compatible avec _CORINE land cover_, ainsi que par une résolution plus élevée. 
* Sur les seules communes des SCOT littoraux : _Littoral néo-aquitain : Occupation du sol (OCS) à Grande Echelle_, millésimes 1985, 2000, 2009 et 2015 (voir les métadonnées [sur le catalogue Pigma](https://www.pigma.org/geonetwork/srv/fre/catalog.search#/metadata/f19d426e-64b7-4c11-848d-759e7d48d62f)).
* Sur l'ensemble de la région : le _Référentiel régional d'OCcupation du Sol (OCS) de la Nouvelle-Aquitaine_, millésimes 2009, 2015 et 2020 (voir les métadonnées [sur le catalogue Pigma](https://www.pigma.org/geonetwork/srv/fre/catalog.search#/metadata/59452557-ca7a-473e-9ed3-c319f515ca5c) et sur [l'observatoire NAFU](https://observatoire-nafu.fr/espaces_nafu/occupation-du-sol/)).

</details>

***

## 🚧 Exercice 7 - Construire des taches urbaines à partir d'une couche décrivant le bâti

### 🗃 Données nécessaires 

* _Shapes_: `Bati_Eyre_2004.shp, Bati_Eyre_2014.shp`

### 🧭 Consignes

1. Charger les deux _shapefiles_, extraits d'une couche 'bâti' de deux millésimes de la BD Topo sur la communauté de commune du Val de l'Eyre. Inspecter leurs tables attributaires.
2. Les mettre en relation par l'outil `Agréger`, (`union` en VO) de manière à construire une symbologie permettant de visualiser les évolutions entre les deux millésimes. Zoomer sur un quartier. Inspecter la forme des transitions: que traduisent-elles ? (Voir p. 70 de la [documentation IGN](http://professionnels.ign.fr/sites/default/files/DC_BDTOPO_2-1.pdf))  
3. En se basant sur le seuil retenu par l'INSEE pour définir ses _unités urbaines (voir la définition [ici](https://www.insee.fr/fr/metadonnees/definition/c1501)), construire des taches urbaines sur les deux dates par une méthode de dilatation-érosion: appliquer un `tampon` positif de la valeur du seuil INSEE divisée par deux (avec union des résultats), puis un `tampon` négatif (avec la même valeur numérique) sur le résultat. 
4. Refaire la même chose avec un outil dédié de l'_Arctoolbox_ : `Cartographie > Généralisation > Délimiter les zones construites`(voir [l'aide d'ArcGIS](http://resources.arcgis.com/fr/help/main/10.2/index.html#/na/007000000047000000/)). Comparer visuellement avec la méthode précédente. Croiser ces deux nouvelles couches (en copiant les symbologies déjà créées). Comparer avec le produit de base, puis avec les taches urbaines issues des bases 'OccSol'. 

***

## 🚧 Exercice 8 - Analyser les évolutions de la réserve naturelle du Banc d'Arguin

### 🗃 Données nécessaires 

* _Shapes_: `N_ENP_RNN_S_000.shp`
* _Décrets_: docs PDF

La réserve naturelle nationale (RNN) du Banc d'Arguin, située à l'embouchure du Bassin d'Arcachon, a un nouveau décret depuis 2017, qui modifie à la fois le réglement et l'emprise de l'aire protégée. 
Plus de renseignements sur les sites des [réserves naturelles de France](http://www.reserves-naturelles.org/banc-d-arguin), de la [SEPANSO](https://www.sepanso.org/nos-missions/la-reserve-naturelle-du-banc-darguin/), de la [préfecture](https://www.gironde.gouv.fr/Actions-de-l-Etat/Environnement-risques-naturels-et-technologiques/Reserves-Naturelles-Nationales-en-Gironde/RNN-du-banc-d-Arguin-RNN5), ainsi que dans un [long format (en ligne, derrière _paywall_)](https://www.sudouest.fr/gironde/arcachon/la-saga-du-banc-d-arguin-reserve-naturelle-depuis-50-ans-11787491.php) du Journal _Sud-Ouest_.

![](Figures/carte_arguin_2015_03.png){: .shadow}
>La réserve en 2015 (source SEPANSO)

### 🧭 Consignes
1. Inspecter les emprises de la RNN sur les _shapefiles_ du dossier 'bonus' (source INPN, mars 2017 ET juillet 2022, soit pré et post décret 2017).
2. Consulter les documents des décrets de 1986 et 2017, ainsi que les articles parus dans Sud-Ouest sur le sujet depuis fin 2021.
3. Lire les arrêtés de délimitation des _Zones de Protection Intégrale_ de la réserve datant de 2012, 2019, 2023 et 2024. 
4. Observer le traitement de la réserve dans les rendus de la [cartographie maritime du SIBA](http://geo.bassin-arcachon.com/e-navigation/index.html). Rechercher les zonages de la réserve dans les pages de l'INPN consacrées à l'information géographique, avec téléchargement éventuel des couches ou consommation de webservices (entrées [RNN](https://inpn.mnhn.fr/telechargement/cartes-et-information-geographique/ep/rnn) et [ZPR](https://inpn.mnhn.fr/telechargement/cartes-et-information-geographique/ep/zpr) notamment). La lisibilité institutionnelle de ces zonages vous paraît-elle assurée? 
 


***







