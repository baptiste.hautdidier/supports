#### Supports pour interventions :

* 🌊 **M2 Sciences de la mer** de l'Université de Bordeaux, module *Gestion Intégrée des Zones Cotières* : [Applications SIG, support](TD_GIZC_Hautdidier2018.md)
* 🗺️ **M1 Innovation territoriale et expérimentation** de l'Université Bordeaux-Montaigne, module *De la donnée à l'information géographique*: [Liens et compléments d'information](DIG_Hautdidier.md)
* ✂️ Une page de rappel sur les opérations de [géotraitement](geotraitements.md)
