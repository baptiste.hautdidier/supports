## Bonus
[![DOI](https://zenodo.org/badge/113714660.svg)](https://zenodo.org/badge/latestdoi/113714660)
![DOI](Figures/DOI_logo.svg)(https://doi.org/10.1016/j.landusepol.2016.04.026)

<script id="asp-embed-script" data-zindex="1000000" type="text/javascript" charset="utf-8" src="https://spark.adobe.com/page-embed.js"></script><a class="asp-embed-link" href="https://spark.adobe.com/page/rtjWHN5dLMVvH/" target="_blank"><img src="https://spark.adobe.com/page/rtjWHN5dLMVvH/embed.jpg?buster=1543425105742" alt="Le marais de Reysson" style="width:100%" border="0" /></a>
