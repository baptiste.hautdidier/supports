## Quelques rappels sur les relations topologiques

(Pour mémoire... La compréhension des relations topologiques aide à lever la confusion souvent faite entre requêtes spatiales et géotraitements)

### Les prédicats spatiaux

Le modèle des neuf intersections,  récemment standardisé sous le nom de [DE-9IM](http://en.wikipedia.org/wiki/DE-9IM) permet de décrire un éventail de relations topologiques entre deux géométries.  La matrice suivante décrit les dimensions des neuf intersections possibles pour les géométries a et b, sachant que chacune des géométries est définie par un intérieur, une limite et un extérieur.

... | Intérieur | Limite | Extérieur
----|-----------|--------|----------
Intérieur | dim(I(a) n I(b)) | dim(I(a) n  L(b)) | dim(I(a) n E(b))
Limite | dim(L(a) n I(b)) | dim(B(a) n L(b)) | dim(L(a) n E(b))
Extérieur | dim(E(a) n  I(b)) | dim(E(a) n L(b)) | dim(E(a) n (b))

Les valeurs possibles de chaque intersection n dans la matrice sont définies par convention : 

* -1 (intersection nulle)
* ø (l’intersection est ponctuelle)
* 1 (l’intersection est linéaire)
* 2 (l’intersection est surfacique)

En prenant  l’exemple de deux polygones se recouvrant partiellement, on obtient la configuration suivante :

![DE9IM](Figures/DE9IM.png)
> Le modèle DE-9IM (source: Wikipedia)

S’il existe un grand nombre de combinaisons potentielles, la plupart ne sont pas réalisables, d’autres sont pratiquement équivalentes. Il est possible de rassembler ces matrices pour construire des prédicats spatiaux en nombre beaucoup plus réduit ; et qui traduisent des attentes par rapport à certaines relations topologiques. 

Le cas simple de l’inclusion d’une géométrie A dans une géométrie B se traduit par les exigences suivantes sur les valeurs de la matrice (avec Faux ->-1, Vrai-> [0 ;1 ;2]):

... |... | Intérieur | Limite | Extérieur
----|----|-----------|--------|----------
_Within (inside)_ | Intérieur |  __Vrai__ | Indifférent | __Faux__
A est à l’intérieur  | Limite | Indifférent | Indifférent | __Faux__
de B |  Extérieur | Indifférent | Indifférent | Indifférent

Voir la fiche Wikipédia pour un aperçu des prédicats spatiaux décrits par le modèle DE-9IM. Ce standard n’est pas encore d’usage systématique dans les logiciels SIG de bureau – mais il permet par contre de comprendre par soi-même deux types d’opération que font ces SIG :

* _Les requêtes spatiales_, qui travaillent sur les entités sans les modifier, en mobilisant les prédicats spatiaux (ou opérateurs spatiaux)
* _Les opérations de géotraitement_, qui modifient les entités ou en crééent de nouvelles, en mobilisant comme briques de base des portions de ces entités, descriptibles via la matrice DE-9IM.



### Les requêtes spatiales dans QGIS

![](Figures/DubeEgenhofer.png)
> Les prédicats spatiaux (selon Dube et Egenhofer)}

Les prédicats spatiaux possibles sont compatibles avec ceux du standard :

* `Est Égal` (_equals_) : les géométries A et B sont topologiquement égales
* `Est Disjoint` (_disjoint_) : l’intersection des géométries A et B est nulle
* `Touche` (_touches_) : la géométrie A a au moins un point ‘limite’ en commun avec B, mais pas de point ‘intérieur’ 
* `Croise` (_crosses_) : l’intersection des géométries A et B est une géométrie de dimension d’un niveau immédiatement inférieur à la dimension maximum des géométries sources - et elle se trouve à l’intérieur
* `Chevauche` (_overlaps_) : l’intersection des géométries A et B est une géométrie différente – mais de même dimension
* `Intersecte` (_intersects_) : l’intersection des géométries A et B n’est pas nulle
* `À l’intérieur` (_within_) : la géométrie A est complètement incluse dans la géométrie B.


### Les géotraitements dans QGIS
Voir dans les sous-menus de `vecteur`, en particulier à `Outils de géotraitement` mais également à `Outils de gestion de données`. 

On prendra soin à noter la différence entre les outils travaillant...

sur les relations entre 2 couches  | sur une seule couche
-----------------------------------|-------------------
`Découper` | `Tampon`
`Différencier` | `Regrouper`
`Intersection` et `Union` | `Enveloppe convexe`
`Différenciation symétrique` | ...

