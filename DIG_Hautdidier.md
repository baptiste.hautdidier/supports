![](Figures/HeaderDIG.png)

>__De la donnée à l'information géographique__ 
>
> Baptiste Hautdidier
>
> Liens et compléments d'information pour l'UE MRSM32, 2022-2023
>
> (Masters 1 *Gestion territoriale du développement durable* et *Territoires, Images et Environnement*, Université Bordeaux-Montaigne)
>
> 20/09/2022

***

>>>

:warning: Les liens de téléchargement de fichiers pour les premières séances : 
- Un [fichier](https://filesender.renater.fr/?s=download&token=d97cf6bf-92fa-4639-a6d3-b35fe9dbf8bc) comprenant une couverture Lidar de la Leyre datant d'avril 2015 (+ en rappel les couches de la séance 1). Les fichiers `*.7z` sont à extraire avec le logiciel [7-zip](https://www.7-zip.fr/).
- séance 5 : un [fichier](https://filesender.renater.fr/?s=download&token=a8aa28da-666c-4d0f-8f82-ffdac9815ffd) avec trois sources de couches linéaires 'randonnée/cyclisme' : les couches du PDIPR Gironde sur le PNRLG (2016), les itinéraires recensés par le SirtAqui, les traces VTT du site VTTrack + [une autre version shapefile de ce dernier](https://filesender.renater.fr/?s=download&token=843d56e8-c54c-451e-9d74-03e14eb43431) 
- séance 4 : [un peu de lecture](https://filesender.renater.fr/?s=download&token=4bdf4f0e-0708-4207-9e47-93426a54b6b6), 3 articles sur la question de l'écologisation et des pratiques de pleine nature, 2 rapports sur la pratique du VTT et les conflits avec la chasse (valable jusqu'au 17 novembre)
- séance 3 :  :national_park: les [données du PNRLG](https://filesender.renater.fr/?s=download&token=ff560630-9814-449d-972f-cbb1c85c8ebc) (valable jusqu'au 10 novembre)
- séance 2 : un [complément](https://filesender.renater.fr/?s=download&token=fc6087a4-4b91-4568-a0f3-0001149441bb) pour la réalisation d'une carte de situation (valable jusqu'au 27 octobre)
- séance 1 : les [emprises du PNR et OCS CORINE 2018 + diapos du CM du 14/09](https://filesender.renater.fr/?s=download&token=3c3b1b4d-12ed-4512-8239-58a8bab25a39)  + [les diapos du 20/09](https://filesender.renater.fr/?s=download&token=006e4fce-f5fd-4429-b344-13575869d414) (valable jusqu'au 20 octobre)

Des liens vers deux tutoriels QGIS :
- [Celui](https://ouvrir.passages.cnrs.fr/tutoqgis/) de l'UMR Passages, réalisé par Julie Pierson.
- [SIGEA](https://sigea.educagri.fr/tutos-sig/tutos-qgis), émanant de l'enseignement agricole 



>>>
## À propos des attendus du module


### Trois objectifs...

- Pratique avancée d'**outils et méthodes** de la géographie numérique (SIG, cartographie, analyse spatiale…) 
- Acquisition d’un **regard critique** sur les usages de l’information géographique (de la création à la mise à disposition et traitement des données)
- Mise en **situation professionnelle** auprès d’un partenaire extérieur (en l’occurrence le PNR des Landes de Gascogne)	


### Les jalons des UE

Les travaux du module se font en continuité avec l'UE du second semestre  : 

1. à la mi-parcours de nos 12 séances, une **présentation collective** de votre travail : problématique générale et objectifs, avec un organigramme de données
3. En début du second semestre, un **pré-rapport** d'étape, qui servira de base à...
4. Un **rapport collectif** en fin de module, et à...
5. Une participation exhaustive à la **restitution (orale) des travaux auprès du PNR**


### Contenu du rapport final

Le rapport devrait avoir une structure de ce type :   

1. Choix du sujet et mise en problématique
2. Cheminement méthodologique présentant les hypothèses et analyses proposées, avec un organigramme théorique
3. Traitements réalisés et résultats, avec organigramme(s) opérationnel(s)
4. Discussion et perspectives   

Les résultats n'ont pas à être exhaustifs, le plus important étant d'abord de bien expliquer votre méthode, de manière suffisamment détaillée pour qu'on puisse en comprendre l'intérêt et la reproduire, notamment grâce aux organigrammes.


### Les organigrammes des données

…ou diagrammes-flux (_flowcharts_). Ce sont des éléments importants du rapport car ils permettent de mettre en évidence les données que vous souhaitez mobiliser, le détail des opérations que vous leur appliquez (ou souhaitez leur appliquer) et leur enchainement logique. 

<details><summary>À titre d’exemple... :speech_balloon:</summary>

voici un diagramme détaillé qui décrit la création d'un découpage adhoc de la base 'occupation du sol' Corine Land cover sur l'emprise du PNR des Landes de Gascogne :

![flowchart](Figures/flowchart_CLC.png)
>  Un exemple d'organigramme de données

</details>

Il y a beaucoup de figurés possibles, nous ne serons pas très pointilleux sur leur choix détaillé. Ce qui semble fondamental par contre, c’est qu’ils permettent de bien séparer dans vos diagrammes ce qui tient de la **donnée** (parallélogramme en général, rectangle « plié arrondi » pour de la donnée stockée, cylindre pour une BD…) de l’**opération** (rectangle pour un processus, losange pour une décision, trapèze pour une opération manuelle…). Beaucoup d’éditeurs sont disponibles. La 1<sup>ère</sup> possibilité reste MS Office ou Libre Office mais vous pouvez aussi essayer des applications web gratuites. 

### Compléments
<details><summary> Pour dessiner des organigrammes :art: </summary>

Trois outils dédiés gratuits : 

* *[yEd](https://www.yworks.com/products/yed/download#download)*, utilisé pour le diagramme vu plus haut
* *[Diagrams.net](https://workspace.google.com/marketplace/app/diagramsnet/671128082532)*, précédemment *draw.io*, module complémentaire de *Google Drive*
* *[LucidChart](https://workspace.google.com/marketplace/app/lucidchart_diagrams/7081045131)*, module complémentaire de *Google Drive*, intégré à *Sheets*, *Docs* et *Slides*
</details>

<details><summary> Pour reproduire la manipulation décrite par ce 'flowchart' :construction: </summary>

Il vous faut :

1. Télécharger le produit ADMIN EXPRESS sur le site [geoservices IGN](https://wxs.ign.fr/x02uy2aiwjo9bm8ce5plwqmr/telechargement/prepackage/ADMINEXPRESS_SHP_WGS84G_PACK_2022-09-20$ADMIN-EXPRESS_3-1__SHP__FRA_WM_2022-09-20/file/ADMIN-EXPRESS_3-1__SHP__FRA_WM_2022-09-20.7z)
2. 7-zip, le logiciel qui vous permettra de décompresser l'archive, est téléchargeable [ici](https://www.7-zip.fr/)
3. Dans l'archive, c'est le fichier `...ADMIN-EXPRESS_3-0__SHP__FRA_2021-08-17\ADMIN-EXPRESS\1_DONNEES_LIVRAISON_2021-08-17\ADE_3-0_SHP_LAMB93_FR` dont vous aurez besoin.
4. les autres couches et le tableau excel sont dans le lien de téléchargement *filesender* présenté en haut de page. 
</details>


***





## Aspects thématique pour 2022-2023



Nos interlocuteurs au PNR des Landes de Gascogne :
- Frédéric Gilbert, responsable "développement des filières de randonnée et régulation des pratiques consommatrices d'espaces", pôle *Tourisme et marque, sports de nature*
- Jean Servant, géomaticien, pôle *Administration et fonctions supports*

:zap: **à consulter absolument** :zap: : [la charte 2014-2026 du parc](https://www.parc-landes-de-gascogne.fr/Parc-Naturel-Regional-de-Gascogne/Votre-parc/Presentation/La-Charte-du-Parc)



### La 'commande' du PNR des Landes de Gascogne

Elle porte, pour 2022-2023, sur un besoin de réflexion sur la **fréquentation des milieux naturels par les pratiques de pleine nature** :

* Pratiques sportives, individuelles, de club, de prestataires
* Evènements 'outdoor'
* Balades, usages 'traditionnels'

Mise en valeur des flux, identification des zones par pratiques, saisonnalité, conflits d'usage et zones sous tension...

[...]

Sources possibles:

* Éco-compteurs du parc (infrarouge, ayant pour certains la capacité à distinguer les véhicules)
* Habitats/zonages
* RES : Recensement des Équipements sportifs
* Les outils départementaux du PDESI (Plan Départemental des Espaces Sites et Itinéraires) et du PDIPR (Plan Départemental des Itinéraires de Promenade et de Randonnée), déclinés côté [Gironde](https://www.gironde.fr/collectivites/culture-sport-vie-associative/espaces-sites-et-itineraires-relatifs-aux-sports-de#gestionsitesetitineraires) et [Landes](https://rando.landes.fr/).
* Les traces GPX: Outdoorvision, Strava, Visorando, VisuGPX, Openrunner,IGN rando, etc...


à consulter : 

* [L'outil de porter à connaissance du Parc naturel régional des Landes de Gascogne](https://pnr-lg.maps.arcgis.com/apps/MapSeries/index.html?appid=1c326a4e87c948d6b35fa5269564b908) (sous forme de *story map* ESRI - voir en particulier l'entrée "éco tourisme et activités culturelles > Capacité et fréquentation touristique")
* Le site [Canoë sur la Leyre](https://www.canoesurlaleyre.com/). 

## Les données des pratiques de pleine nature

### Les circuits officiels

Cette offre est en fait plus difficile à qualifier qu'il n'y paraît. Il y a une forme de hiérarchisation, les itinéraires locaux étant les moins suivis et les moins publicisés. On est surtout sur un bel exemple de données construites avec la sphère publique, mais qui ne sont pas restés en *open data* pour autant : les tracés du PDIPR de la Gironde ne sont ainsi plus disponibles en téléchargement libre, alors qu'ils l'étaient en 2016-1017. (*Cf.* le fichier `zip` de la *séance 5*).


Les PDESI identifient les sites sur lesquels le département met une priorité, *Cf.* cette [carte](https://www.gironde.fr/sites/default/files/2017-02/carte%20des%20espaces%20sites%20et%20itin%C3%A9raires%202015.pdf). Pour la partie Girondine du PNRLG, il s'agit :
- du Domaine départemental Gérard Lagorce à Hostens (avec boucles VTT, trail et équitation)
- de la Leyre entre Biganos et Belin-Béliet (40 km en canoë)  

**Du côté de la FFC** (cyclisme), on retrouve cette logique de site, avec Hostens pour le VTT. Le site étant fermé suite aux incendies, la [page dédiée](https://sitesvtt.ffc.fr/sites/domaine-departemental-gerard-lagors-hostens-6/) ne permet pas le télécxhargement des traces GPX, par ailleurs repérables sur une [brochure](https://network.ffc.fr/app/uploads/sites/2/2019/10/hostens-blasimon-depliant-2015.pdf).

**Du côté de la FFRando**, on trouve des descriptifs des chemins de grande randonnée, qui ne sont que 4 sur le PNRLG : 
* GR® 6 entre Alpes provençales et Arcachon : https://www.mongr.fr/trouver-prochaine-randonnee/itineraire/gr-6-entre-alpes-provencales-et-arcachon 
* GR® 655 : Compostelle via Turonensis : https://www.mongr.fr/trouver-prochaine-randonnee/itineraire/gr-655-compostelle-via-turonensis 
* GR® 654O de La Réole à Mont-de-Marsan : https://www.mongr.fr/trouver-prochaine-randonnee/suggestion/gr-654o-de-la-reole-a-mont-de-marsan
* GR® de Pays Tour du Bassin d'Arcachon : https://www.mongr.fr/trouver-prochaine-randonnee/suggestion/gr-de-pays-tour-du-bassin-d-arcachon

**Du côté de l'office du tourisme**, il y a quelques boucles locales et itinéraires :
* la boucle de Mauriac, à Saint-Symphorien : https://fr.calameo.com/gironde-tourisme/read/0002587134e224f4f9041 et https://goo.gl/NSYLec
* boucles de certes et Graveyron : https://fr.calameo.com/gironde-tourisme/read/0002587137e4f1db96c7e et https://goo.gl/C2Djaf
* https://www.gironde-tourisme.fr/itineraires/au-teich-comme-un-oiseau-sur-le-sentier-du-littoral/
* https://www.gironde-tourisme.fr/itineraires/au-teich-du-domaine-de-fleury-au-domaine-des-4-paysans/
* https://www.gironde-tourisme.fr/itineraires/a-audenge-entre-terre-et-eaux-sur-le-domaine-de-graveyron/
* https://www.gironde-tourisme.fr/itineraires/a-mios-la-petite-boucle-du-val-de-leyre-ouest/
* https://www.gironde-tourisme.fr/itineraires/fermee-temporairement-petite-boucle-autour-des-lacs-dhostens/
* https://www.gironde-tourisme.fr/itineraires/boucle-francois-mauriac-un-adolescent-dautrefois/

Dans les Landes, l'offre est présentée via un [visualiseur](https://rando.landes.fr/). Toutefois, l'existence de guides papier vendus 2€ est un frein à la consultation et au partage des parcours. Autre point, un partage des tâches plus exclusif entre département et PNR (comme en témoigne la localisation hors parc des [circuits VTT du CD40](https://www.landes.fr/files/cg40/rando/Landes_circuits_VTTbd.pdf)).



### Traces géonumériques

A defaut d'accéder sans restriction aux traces déposées sur la plateforme *Outdoor Vision*, il est possible de se constituer une première base de travail via deux sources :   

* [SirtAqui](https://www.sirtaqui-aquitaine.com/vous-souhaitez-r%C3%A9utiliser-des-donnees/), pour 'Système d’Information Régional Touristique de Nouvelle-Aquitaine' : près de 2600 fichiers GPX sur la région, dont une centaine sur le PNR. Un inconvénient, la quasi-absence de métadonnées pour distinguer les types de pratiques.
* [VTTrack](http://www.vttrack.fr/), un site qui a le grand avantage de moissonner plusieurs sources (Openrunner, Utagawa, VisuGPX, traceGPS...) sur un visualiseur unique : 158 traces sur le PNR, sur la seule pratique du VTT.

Ces deux sources sont incluses dans le fichier `zip` de la *séance 5*.

- Méthode de constitution des couches Sirtaqui: via [une liste](https://www.sirtaqui-aquitaine.com/app/download/14508818622/SIRTAQUI_Liste_jeux_donnees_liberes_open_data_2022_V2.pdf?t=1666085113) des jeux de données libérés en *open data*, téléchargement d'un [fichier CSV](http://aquitaine.media.tourinsoft.eu/upload/SIRTAQUI-OD-liste-csv-itineraires.csv) décrivant les itinéraires, export d'une colonne décrivant les URLS des fichiers GPX mis en ligne, conversion en script `R` pour un téléchargement automatique avec la fonction `wget`, assemblage de l'ensemble sous QGIS avec l'extension `batch GPS importer`.
- Méthode de constitution des couches VTTrack: après inspection des données structurées visibles dans l'onglet `Outils de développement` sous `Chrome`, construction d'un lien de téléchargement dans le navigateur par inclusion de l'adresse d'un service WFS et une zone d'intérêt (par ex. http://www.vttrack.fr/cgi-bin/mapserv.fcgi?map=/srv/d_vttrack/vttrack/production/mapserver/WFS-utagawa-postgis.map&SERVICE=WFS&VERSION=1.1.0&TYPENAME=utagawa&REQUEST=GetFeature&BBOX=-1.11,43.9,0,44.9)





## Quelques pistes

- Les incendies de cet été ont entrainé la fermeture du site d'Hostens et ses itinéraires : quelles configurations imaginer pour la réouverture ? à l'identique dans les parcelles brûlées, ou ailleurs ?
- Pour la pratique du vélo, il est intéressant de regarder les tendances récentes, avec des craintes plus ou moins étayées sur des impacts possibles : VAE d'un côté (démocratisation de la pratique et questionnement sur le mérite), *gravel* et *bikepacking* de l'autre (développement de pratiques de bivouac non encadré?).
- Autres sites/applications à regarder de près : `Strava` et `Komoot`. En examiner les logiques : segments compétitifs pour `Strava`, planification d'itinéraires pour `Komoot`. Nous verrons en séance 5 pour d'éventuelles extractions. 
- Le parking de nuit de vans ou camping cars est une autre pratique évoquée à la prise de commande : regarder éventuellement le site et l'application `park4night`

### Les incendies de l'été 2022

![](Figures/IncendieSaumos2022.png)

Comme en témoigne cette première estimation de la séverité des dégâts lors de l'incendie survenu à Saumos en septembre 2022, l'ONF et la DFCI se basent sur une analyse des bandes infrarouges de sources satellitaires (`Sentinel-2` en l'occurrence). L'indice utilisé est le _Normalized Burn Ratio_ (NBR) : 

```math
NBR = \frac{NIR - SWIR}{NIR + SWIR}
```
où 
- NIR : reflectance dans l'infrarouge proche (Bande `Sentinel-2 B8a`)
- SWIR : reflectance dans l'infrarouge court (Bande `Sentinel B12`)

Le _Differenced NBR_ (dNBR), appliqué à des images pré- et post- évènement, permet d'avoir un indice en principe assez bien corrélé à l'intensité du passage de feu. 

```math
dNBR = NBR_{avant-feu} - NBR_{après-feu}
```
Il est possible de construire une chaine de traitement sur QGIS qui calcule cet indice à partir de 2 images Sentinel-2 :
téléchargement possible via le [site Theia](https://theia.cnes.fr/atdistrib/rocket/#/search?collection=SENTINEL2), en choisissant la tuile `T30TXQ` et des images composites mensuelles `N3A` en début et fin d'été (`juin`, `septembre`)
- https://theia.cnes.fr/atdistrib/resto2/collections/SENTINEL2/7a2179cc-5c5b-5675-9cfa-d8718ce34538/download?_tk=78efb4fea2c264f4fe1bb116e14e2d667666bab2
- https://theia.cnes.fr/atdistrib/resto2/collections/SENTINEL2/77da947d-a6eb-5466-8d87-4aff331c7734/download?_tk=8e20bc50358b2582e403f519015c842d29bdf203


### OpenStreetMap comme source

Les parkings sont identifiés dans OSM via la catégorie `amenity=parking`. L'étiquette `tourism=caravan_site` pourrait avoir une certaine utilité https://wiki.openstreetmap.org/wiki/FR:Tag:tourism%3Dcaravan_site Les spots pour camping cars ont fait l'objet de vélléités d'import, ce qui n'a pas été sans poser problèmes, comme en témoignent ces [échanges sur le forum d'OSM](https://forum.openstreetmap.fr/t/lieux-de-stationnement-pour-camping-car-pollution-de-la-base/8709/21).

De même, les pistes cyclables sont bien représentées. L'effort porte plus généralement sur une cartographie du réseau cyclable, _i.e._ des portions de réseau routier dont des voies permettent une pratique en principe plus sécurisée du vélo (bandes cyclables, couloirs partagés avec les bus, trottoirs avec aménagements cyclables, voies vertes...). Voir [ici](https://wiki.openstreetmap.org/wiki/FR:Bicycle) pour une description des catégories. Cf. également un [article](https://halshs.archives-ouvertes.fr/halshs-00841777/document) de Jérôme Denis et David Pontille. Pour les itinéraires VTT, une [discussion](https://wiki.openstreetmap.org/wiki/FR:Cyclisme_tout_terrain) sur un étiquetage dédié a été lancée au sein de la communauté OSM. Deux interfaces en ligne pour télécharger la donnée OSM : 

- [magOSM](https://magosm.magellium.com/portail/#/carte): "services de données thématiques issues d'OpenStreetMap" 
- [OSM DATA](https://demo.openstreetmap.fr/map): "la donnée d'OpenStreetMap". Plus d'options de formats pour les exports de données. 

## Les sources communes

* IGN : les données libres sont accessibles via la [page de téléchargement](https://geoservices.ign.fr/telechargement) et le [catalogue](https://geoservices.ign.fr/catalogue)
* Les [cartes et information géographique](https://inpn.mnhn.fr/telechargement/cartes-et-information-geographique) de l'INPN (Muséum) 
* SIGENA: https://www.sigena.fr/accueil
* Le [catalogue](https://portail.pigma.org/outils-et-services/catalogue/) et le [visualiseur](https://www.pigma.org/mapfishapp/) de PIGMA
* CORINE *Land Cover* (1990, 2000, 2006, 2012, 2018), disponible au niveau [national](https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0) ou [régional](http://www.donnees.statistiques.developpement-durable.gouv.fr/donneesCLC/CLC/region/CLC_RALPC_RGF_SHP.zip)
* Une OCS à grande échelle, le [*Référentiel néo-aquitain d’Occupation du Sol*](https://portail.pigma.org/groupe-de-travail/millesime-2015-du-referentiel-regional-doccupation-du-sol-ocs-est-disponible-pour-les-departements-d24d33d40d47d64/) (2000, 2009, 2015, et 2020 en Beta accessible via PIGMA) 
* [OSO](https://www.theia-land.fr/ceslist/ces-occupation-des-sols/), une OCS automatisée (annuelle depuis 2016...): 
* Le [cadastre](https://cadastre.data.gouv.fr/datasets/cadastre-etalab) Etalab : 



Des sources de données satellitaires possibles : 

* Spot 6-7 : https://www.theia-land.fr/en/product/spot-6-7/
* Venµs : https://theia.cnes.fr/atdistrib/rocket/#/search?page=1&collection=VENUS&location=ESGISB-3
* Sentinel-2 : https://theia.cnes.fr/atdistrib/rocket/#/search?collection=SENTINEL2



## Les services web

### WMS
pour _Web map services_ : cartes raster, avec une symbologie déjà définie
* IGN : https://geoservices.ign.fr/services-web-experts
* Pigma : https://www.pigma.org/geoserver/wms
* Sigena : https://datacarto.sigena.fr/cgi-bin/mapserv?
* ARB Nouvelle Aquitaine (ex SIGORE):  https://wms.biodiversite-nouvelle-aquitaine.fr/wms_arbna
* Agence de l'eau Adour-Garonne : http://adour-garonne.eaufrance.fr/servicesOGC?

### WFS
pour _Web feature services_ : couches vectorielles, exportables en local
* Pigma : https://www.pigma.org/geoserver/wfs
* Sigena : https://datacarto.sigena.fr/cgi-bin/mapservwfs?
* Agence de l'eau Adour-Garonne : http://adour-garonne.eaufrance.fr/servicesOGC?

### WCS
pour _Web coverage services_ : couches raster brutes, exportables en local
* Pigma : https://www.pigma.org/geoserver/wcs






***

# Compléments

(après CM du 27/09/2022)

**1. Théorème de Bayes et sous-marins** 

* Un [article](https://www.amq.math.ca/wp-content/uploads/bulletin/vol58/no3/05-Bayes.pdf)
d'un bulletin de mathématique, qui traite de l'exemple de l'USS Scorpion et des méthodes statistiques utilisées pour la recherche de l'épave
* [*The theory that would not die*](https://books.google.fr/books?id=_Kx5xVGuLRIC&lpg=PP2&ots=L-Zrv6SSCe&lr&hl=fr&pg=PR7#v=onepage&q&f=false), livre sur les fortunes diverses des méthodes bayesiennes.

**2. Sur le *Sharpiegate***
* L'[interview](https://www.citylab.com/design/2019/09/trump-sharpiegate-map-dorian-noaa-forecast-alabama-facts/597781/) de Mark Monmonier, auteur du classique [*Comment faire mentir les cartes*](https://www.autrement.com/Catalogue/atlas/atlas-concours/comment-faire-mentir-les-cartes).
* Le [commentaire](http://www.thefunctionalart.com/2019/09/mark-monmonier-talks-about-sharpiegate.html) d'Alberto Cairo, ainsi que sa tribune basée sur une [visualisation interactive](https://www.nytimes.com/interactive/2019/08/29/opinion/hurricane-dorian-forecast-map.html).

**3. *5000 feet is the best* d'Omer Fast**
* Une version complète (30 min) de ce court-métrage, avec sous-titrages automatisés, a été mise en ligne sur [*Youtube*](https://www.youtube.com/watch?v=K-8dW1dg7KY). Le film, qui est basé sur l'interview d'un ex-opérateur de drone, alterne prises de vues d'un acteur dans un hotel avec des plans en extérieur + témoignage en voix *off* (Pour ces extraits, voir à 6:18, 14:55, 20:30 et 27:00)
* Des extraits sont sur [Viméo](https://vimeo.com/34050994) ou le site du [*Guardian*](https://www.theguardian.com/artanddesign/video/2013/jul/25/drone-iwm-contemporary-omer-fast-art-video).

**4. Julien Prévieux**
* Le film *Patterns of life* est visible sur [Youtube](https://www.youtube.com/watch?v=c45IfGRkJ_w&ab_channel=Op%C3%A9ranationaldeParis) et [Vimeo](https://vimeo.com/141794173). Les textes lus ont été sélectionnés en collaboration avec Grégoire Chamayou, sur base notamment de [ce papier](https://thefunambulist.net/history/the-funambulist-papers-57-schematic-bodies-notes-on-a-patterns-genealogy-by-gregoire-chamayou) dans la revue *The Funambulist*. 
* Voir aussi le [site de l'artiste](https://www.previeux.net/). En particulier : [*Where is my (deep) mind?*](https://www.previeux.net/fr/videos_WIMDM.html), court film sur le *machine learning*, ainsi que l'[*atelier de dessin avec la BAC du 14e arrondissement de Paris*](https://www.previeux.net/fr/works-atelierbac.html), autour des cartographies du *predictive policing*. 
* Un livre, [*Statactivisme*](https://www.editions-zones.fr/livres/statactivisme/), qu'il a coédité avec Emmanuel Didier et Isabelle Bruno.

**5. Palantir**
* Le portrait d'Alex Karp dans le [New York Times magazine](https://www.nytimes.com/interactive/2020/10/21/magazine/palantir-alex-karp.html), (accessible avec vos identifiants Bordeaux-Montaigne)
* La description de Gotham sur le [site](https://www.palantir.com/platforms/gotham/) de Palantir
* L'[interview](https://www.francetvinfo.fr/replay-radio/l-interview-eco/nous-sommes-un-anti-gafa-nous-ne-collectons-pas-les-donnees-affirme-fabrice-bregier-president-de-palantir-france_5322796.html) sur *France Info* de Fabrice Fergier, président de Palantir France.

**6. Visualisation graphique, design analytique et sémiologie graphique**
* La *Carte figurative des pertes successives en hommes de l'armée française dans la campagne de Russie 1812-1813* de Minard est sur [Gallica](http://gallica.bnf.fr/ark:/12148/btv1b52504201x/f1.item.zoom)
* Le [site web](https://www.edwardtufte.com/tufte/) d'Edward Tufte, gourou minimaliste du design analytique. *The Visual Display of Quantitative Information* est une référence.
* *La sémiologie graphique* de Jacques Bertin n'est à ma connaissance pas en ligne, même si vous pouvez en trouver assez facilement les figures principales. Lecture utile, un retour critique par Gilles Palsky sur le site [*Visions Carto*](https://visionscarto.net/la-semiologie-graphique-a-50-ans), qui à l'occasion du cinquantenaire du livre en montre les apports essentiels, ainsi que les écueils d'un usage trop dogmatique.  
* *Data Visualization: A practical introduction* de Kieran Healy un livre très pédagogique, accessible librement [en ligne](https://socviz.co/). Voir en particulier les §1.3 et 1.4 pour un état de l'art actualisé sur la sémiologie...

**7. Les bonnes pratiques de visualisation graphique et de cartographie du *NY Times***
* Pas une lubie particulière de ma part, le département de datajournalisme  du NYT est depuis 10 ans l'un des plus innovants, sous l'influence notable de sa directrice, [Amanda Cox](https://www.nytimes.com/2019/02/28/reader-center/data-visualization-editor-amanda-cox.html)
* Cartogrammes : http://www.nytimes.com/interactive/2013/04/08/business/global/asia-map.html
* Cartes dasymétriques : http://www.nytimes.com/interactive/2014/11/04/upshot/senate-maps.html
* *Dot maps* : http://www.nytimes.com/interactive/2015/07/08/us/census-race-map.html
* Principe 'bertinien' sur les grilles de symboles proportionnels : http://www.nytimes.com/interactive/2013/01/02/us/chicago-killings.html
* Principe 'tuftien' sur l'intégration des légendes : http://www.nytimes.com/interactive/2014/08/03/world/middleeast/assessing-the-damage-and-destruction-in-gaza.html
* Intégration récit et cartographie via le *scrollytelling* : https://www.nytimes.com/newsgraphics/2013/10/27/south-china-sea/index.html

**8. Références complémentaires***

*En français*
* Joliveau, T., Noucher, M., & Roche, S. (2013). La cartographie 2.0, vers une approche critique d'un nouveau régime cartographique. *L'Information géographique*, 77(4), 29-46, sur [CAIRN](https://www.cairn.info/revue-l-information-geographique-2013-4-page-29.htm).
* Joliveau, T. (2011). Le géoweb, un nouveau défi pour les bases de données géographiques. *L'Espace géographique*, 40(2), 154-163, sur [CAIRN](https://www.cairn.info/revue-espace-geographique-2011-2-page-154.htm)
* Noucher, M. (2015). De la trace à la carte et de la carte à la trace: pour une approche critique des nouvelles sources de fabrique cartographique. Marta Severo; Alberto Romele. *Traces numériques et
territoires*, Presses des Mines, sur [HAL](https://halshs.archives-ouvertes.fr/halshs-01212022/document)

*En anglais*
* Gotway CA & Young LJ (2002) Combining incompatible spatial data. *Journal of the American Statistical Association* 97(458).
* Grevsmühl SV (2016) Images, imagination and the global environment: towards an interdisciplinary research agenda on global environmental images. *Geo: Geography and Environment* 3(2):e00020-n/a.
* Kitchin R, Gleeson J, & Dodge M (2013) Unfolding mapping practices: A new epistemology for cartography. *Transactions of the Institute of British Geographers* 38(3):480-496.
* Lacoste Y (1973) An illustration of geographical warfare: Bombing of the dikes on the red river, North Vietnam. *Antipode* 5(2):1-13.
* McCauley D.J, et al. (2016) Ending hide and seek at sea. *Science* 351(6278):1148-1150. http://science.sciencemag.org/content/351/6278/1148
* Schuurman, N. (2000). Trouble in the heartland: GIS and its critics in the 1990s. *Progress in human geography*, 24(4), 569-590.
* Wilson, M. W. (2017). *New lines: Critical GIS and the trouble of the map*. U of Minnesota Press.
* Wood, D., & Fels, J. (2008). The natures of maps: cartographic constructions of the natural world. *Cartographica: The International Journal for Geographic Information and Geovisualization*, 43(3), 189-202. Sur le [site](http://deniswood.net/content/papers/Wood%20and%20Fels-Natures.pdf) de Denis Wood. 


